from kivymd.app import MDApp
from kivy.lang.builder import Builder
from kivymd.uix.screen import Screen
from kivy.core.window import Window
from kivy.graphics import Rectangle
from kivy.core.audio import SoundLoader
from kivy.clock import Clock
from game import GameWidget
import os
os.environ["KIVY_NO_CONSOLELOG"] = "1"

homeScreen = """
MDFloatLayout:
    Image:
        source: "./images/home.jpg"
        pos: self.pos
        size_hint: None, None
        size: root.size
        allow_stretch: True
        keep_ratio: False
    Image:
        source: "./images/logo.png"
        pos_hint: {'center_x':0.5, 'center_y': 0.8}
    MDRoundFlatIconButton:
        icon: "gamepad"
        text: "Start Game"
        pos_hint: {'center_x':0.5, 'center_y': 0.5}
        on_release: app.to_stages()
    MDRoundFlatIconButton:
        icon: "help"
        text: "Help"
        pos_hint: {'center_x':0.5, 'center_y': 0.4}
        on_release: app.to_help()
    MDRoundFlatIconButton:
        icon: "settings"
        text: "Settings"
        pos_hint: {'center_x':0.5, 'center_y': 0.3}
        on_release: app.to_settings()
"""


stagesScreen = """
MDFloatLayout:
    Image:
        source: "./images/home.jpg"
        pos: self.pos
        size_hint: None, None
        size: root.size
        allow_stretch: True
        keep_ratio: False
    MDLabel:
        text: 'Stage selection'
        pos_hint : {"center_y": 0.95}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDCard:
        orientation: "vertical"
        padding: "8dp"
        size_hint: None, None
        size: "280dp", "180dp"
        pos_hint: {"center_x": .5, "center_y": .725}
        on_release: app.launch_game(key=1)
        MDLabel:
            text: "Aeris-103 (easy)"
            theme_text_color: "Secondary"
            size_hint_y: None
            height: self.texture_size[1]
        Image:
            source: "./images/background1.jpg"
    MDCard:
        orientation: "vertical"
        padding: "8dp"
        size_hint: None, None
        size: "280dp", "180dp"
        pos_hint: {"center_x": .5, "center_y": .40}
        on_release: app.launch_game(key=2)
        MDLabel:
            text: "Exodus-732 (medium)"
            theme_text_color: "Secondary"
            size_hint_y: None
            height: self.texture_size[1]
        Image:
            source: "./images/background2.jpg"
    MDRoundFlatIconButton:
        icon: "home"
        text: "Home"
        pos_hint: {'center_x':0.5, 'center_y': 0.15}
        elevation_normal: 12
        on_release: app.to_home()
"""


helpScreen = """
MDFloatLayout:
    Image:
        source: "./images/home.jpg"
        pos: self.pos
        size_hint: None, None
        size: root.size
        allow_stretch: True
        keep_ratio: False
    Image:
        source: "./images/help.png"
        pos_hint: {'center_x':0.5, 'center_y': 0.7}
    MDLabel:
        text: 'Hold left to move left'
        pos_hint : {'center_x':0.5, 'center_y': 0.40}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDLabel:
        text: 'Hold right to move right'
        pos_hint : {'center_x':0.5, 'center_y': 0.35}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDLabel:
        text: 'Hold center to move up'
        pos_hint : {'center_x':0.5, 'center_y': 0.30}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDRoundFlatIconButton:
        icon: "home"
        text: "Home"
        pos_hint: {'center_x':0.5, 'center_y': 0.15}
        elevation_normal: 12
        on_release: app.to_home()
"""


settingsScreen = """
MDFloatLayout:
    Image:
        source: "./images/home.jpg"
        pos: self.pos
        size_hint: None, None
        size: root.size
        allow_stretch: True
        keep_ratio: False
    Image:
        source: "./images/logo.png"
        pos_hint: {'center_x':0.5, 'center_y': 0.8}
    MDLabel:
        text: 'Audio settings'
        pos_hint : {"center_y": 0.55}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDLabel:
        text: 'Volume'
        pos_hint : {'center_x':0.2, 'center_y': 0.45}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDSlider:
        id : slider
        min: 1
        max: 5
        value: 2
        pos_hint: {'center_x':0.6, 'center_y':.45}
        size_hint: 0.425, 0.2
        on_touch_up : app.set_volume(slider.value)
    MDLabel:
        text: 'Music'
        pos_hint : {'center_x':0.2, 'center_y': 0.35}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDSwitch:
        active : True
        pos_hint: {'center_x':0.34, 'center_y':0.35}
        size_hint: 0.1, 0.08
        on_active: app.set_playSoundBGM(*args)
    MDLabel:
        text: 'Effects'
        pos_hint : {'center_x':0.6, 'center_y': 0.35}
        font_style : "H6"
        halign : "center"
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
    MDSwitch:
        active : True
        size_hint: 0.1, 0.05
        pos_hint: {'center_x':0.76, 'center_y':0.35}
        on_active: app.set_playSoundFx(*args)
    MDRoundFlatIconButton:
        icon: "home"
        text: "Home"
        pos_hint: {'center_x':0.5, 'center_y': 0.2}
        elevation_normal: 12
        on_release: app.to_home()
"""


data = {
    0 : {
        "BGM"   : SoundLoader.load('./sounds/brachis.ogg'),
    },
    1 : {
        "BGM"   : SoundLoader.load('./sounds/level1.ogg'),
        "BGI"   : Rectangle(source='./images/background1.jpg', pos=(0,0), size=(Window.width, Window.height)),
        "bonus" : SoundLoader.load('./sounds/bonus.ogg'),
        "death" : SoundLoader.load('./sounds/death.ogg'),
    },
    2 : {
        "BGM"   : SoundLoader.load('./sounds/level2.ogg'),
        "BGI"   : Rectangle(source='./images/background2.jpg', pos=(0,0), size=(Window.width, Window.height)),
        "bonus" : SoundLoader.load('./sounds/bonus.ogg'),
        "death" : SoundLoader.load('./sounds/death.ogg'),
    },
}


class Brachistonaut(MDApp):

    def build(self):
        self.theme_cls.primary_palette = "Blue"
        self.theme_cls.primary_hue = "500"
        self.theme_cls.theme_style= "Dark"
        self.FPS=60
        self.screen = Screen()
        return self.screen


    def on_start(self):
        """ Run when application is started """
        self.current_screen = None
        Window.bind(on_keyboard=self.back_key_handler)
        self.init_audio_settings()
        self.init_home()
        self.init_stages()
        self.init_help()
        self.init_settings()
        self.init_game()
        self.to_home()


    def init_home(self):
        """ Create the home page """
        self.home = Builder.load_string(homeScreen)
        self.set_sound_theme()


    def init_stages(self):
        """ Initialize the stage selector widget """
        self.stages = Builder.load_string(stagesScreen)


    def init_help(self):
        """ Create the help page """
        self.help = Builder.load_string(helpScreen)


    def init_settings(self):
        """ Create the settings page """
        self.settings = Builder.load_string(settingsScreen)


    def init_game(self):
        """ Initialize the game widget """
        self.game = GameWidget( data=data,
                                exitCallBack=self.to_home,
                                stagesCallBack=self.to_stages,
                                FPS=self.FPS)
        self.game.setAudioSettings(self.playSoundBGM, self.playSoundFx, self.volume)
        self.context = None


    def init_audio_settings(self):
        """ init the audio settings """
        self.volume = 1./5
        self.playSoundBGM = True
        self.playSoundFx = True


    def set_playSoundBGM(self, checkbox, active):
        """ update the background music setting """
        self.playSoundBGM = active
        if not active :
            self.soundTheme.stop()
        else:
            self.soundTheme.play()
        self.game.setAudioSettings(self.playSoundBGM, self.playSoundFx, self.volume)


    def set_playSoundFx(self, checkbox, active):
        """ update the sound effects music setting """
        self.playSoundFx = active
        self.game.setAudioSettings(self.playSoundBGM, self.playSoundFx, self.volume)


    def set_volume(self, volume):
        """ update the audio settings """
        self.volume = (volume-1) / (5-1)
        self.soundTheme.volume = self.volume
        self.game.setAudioSettings(self.playSoundBGM, self.playSoundFx, self.volume)


    def set_sound_theme(self):
        """ Initialize the background music """
        self.soundTheme = data[0]["BGM"]
        if self.playSoundBGM and self.soundTheme:
            self.soundTheme.volume = self.volume
            self.soundTheme.play()


    def check_sound_theme(self, *args):
        """ Initialize the background music """
        if self.soundTheme and self.soundTheme.state=='stop':
            self.soundTheme.volume = self.volume
            self.soundTheme.play()


    def to_home(self, obj=None):
        """ Go to the home screen """
        self.current_screen = "home"
        self.screen.clear_widgets()
        Clock.schedule_interval(self.check_sound_theme, 1./self.FPS)
        self.screen.add_widget(self.home)


    def to_stages(self):
        """ Go to the stage selection screen """
        self.current_screen = "stages"
        self.screen.clear_widgets()
        Clock.schedule_interval(self.check_sound_theme, 1./self.FPS)
        self.screen.add_widget(self.stages)


    def to_help(self, obj=None):
        """ Go to the home screen """
        self.current_screen = "help"
        self.screen.clear_widgets()
        Clock.schedule_interval(self.check_sound_theme, 1./self.FPS)
        self.screen.add_widget(self.help)


    def to_game(self, obj=None):
        """ Start playing the game """
        self.current_screen = "game"
        self.screen.clear_widgets()
        self.soundTheme.stop()
        Clock.unschedule(self.check_sound_theme)
        Clock.schedule_interval(self.game.update, 1./self.FPS)
        self.game.reset(self.context)
        self.screen.add_widget(self.game)


    def to_settings(self, obj=None):
        """ Open the settings screen """
        self.current_screen = "settings"
        self.screen.clear_widgets()
        Clock.schedule_interval(self.check_sound_theme, 1./self.FPS)
        self.screen.add_widget(self.settings)


    def back_key_handler(self, window, key, *args):
        """ Handle the back button """
        if key == 27 :
            if self.current_screen in ["help", "settings", "stages", "game"] :
                if self.current_screen=="game":
                    self.game.handlePause()
                else:
                    self.to_home()
                return True


    def launch_game(self, key):
        """ Launch the game """
        contexts = {
            1: {'level':1, 'nRocks':2, 'spacecraftIdx':1},
            2: {'level':2, 'nRocks':4, 'spacecraftIdx':2},
        }
        self.context = contexts[key]
        self.to_game()


if __name__ == '__main__':
    Brachistonaut().run()
