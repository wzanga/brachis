
from kivy.uix.widget import Widget
from kivy.core.window import Window
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRoundFlatButton
from kivymd.uix.dialog import MDDialog
from kivy.clock import Clock
from particles import Player, Target, Rock
from math import cos, sin



class GameWidget(Widget):

    def __init__(self, data, exitCallBack, stagesCallBack, FPS, **kwargs):
        super().__init__(**kwargs)
        self.FPS = FPS
        self.gravity = (0, -7.81)
        self.data = data
        self.context = None
        self.soundBGM = None
        self.pauseMenu = None
        self.gameOverMenu = None
        self.exitCallBack = exitCallBack
        self.stagesCallBack = stagesCallBack


    def on_touch_down(self, touch, *args):
        """ Handle user input via the joystick """
        if 3 * touch.x < Window.width:
            self.player.action[0] = -6
            self.player.action[1] = -1.2 * self.gravity[1]
        elif 2 * Window.width < 3 * touch.x :
            self.player.action[0] = 6
            self.player.action[1] = -1.2 * self.gravity[1]
        else:
            self.player.action[0] = -1e-1 * self.player.v[0]
            self.player.action[1] = -2 * self.gravity[1]


    def on_touch_up(self,touch, *args):
        """ Handle user input via the joystick """
        self.player.action = [0, 0]


    def update(self, dt):
        """ Main game loop """
        self.checkGameOver()
        self.checkCollisions(dt)
        if self.soundBGM and self.soundBGM.state=='stop':
            self.soundBGM.volume = self.volumeSoundBGM
            self.soundBGM.play()


    def reset(self, context):
        """ Reset to game to an starting configuration """
        self.context = context
        self.level = context["level"]
        self.canvas.clear()
        self.setMedia()
        self.setParticles(context["nRocks"], context["spacecraftIdx"])
        self.resetScore()


    def resetScore(self):
        """ Reinitialise the score to zero """
        self.score = 0
        pos=(Window.width-100, Window.height-100)
        size_hint=(1., 1.)
        self.scoreLabel = MDLabel(text="0", size_hint=size_hint, pos=pos, theme_text_color="Custom", text_color=(1, 1, 1, 1))
        self.add_widget(self.scoreLabel)


    def updateScore(self, value=0):
        """ Update the user score """
        self.score += value
        self.scoreLabel.text = "{}".format(self.score)



    def setAudioSettings(self, playSoundBGM, playSoundFx, volume):
        """ set the settings for the music and sound effetcs """
        self.playSoundBGM = playSoundBGM
        self.playSoundFx = playSoundFx
        self.volumeSoundBGM = volume
        self.volumeSoundFX = max(0, volume-0.15)


    def setMedia(self):
        """ Initialize the background music, image and fx """
        #set the background image
        self.canvas.add(self.data[self.level]["BGI"])
        #set the background music
        if self.playSoundBGM:
            if self.soundBGM :
                if self.soundBGM.status=="stop":
                    self.soundBGM.play()
            else:
                self.soundBGM = self.data[self.level]["BGM"]
                self.soundBGM.volume = self.volumeSoundBGM
                self.soundBGM.play()
        #set the sounds effects
        if self.playSoundFx :
            self.soundTarget = self.data[self.level]["bonus"]
            self.soundDeath =  self.data[self.level]["death"]


    def setParticles(self, nRocks, spacecraftIdx):
        """ Initialize the player, the target and rocks """
        self.target = Target(gravity=self.gravity, radius=0.05, canvas=self.canvas, level=self.level)
        self.player = Player(gravity=self.gravity, radius=0.05, canvas=self.canvas, level=self.level, spacecraftIdx=spacecraftIdx)
        self.rocks = [None] * nRocks
        rockSizes = [0.05, 0.075, 0.1]
        for i in range(nRocks):
            radius = rockSizes[i%len(rockSizes)]
            self.rocks[i] = Rock(gravity=self.gravity, radius=radius, canvas=self.canvas, level=self.level)


    def getPerformance(self):
        """ Return a text corresponding to the number of points """
        if self.score ==0 :
            return "Error 404 skills not found !"
        if self.score ==1 :
            return "One point ? Really ?!"
        elif self.score < 3:
            return "You're Not Even Trying...Are You?"
        elif self.score < 5:
            return "You can do it, human !...Or Maybe Not"
        elif self.score < 10:
            return "Sky is your a limit"
        elif self.score < 15:
            return "You Have The Eyes Of A Scout. Keep Your Eyes Open!"
        elif self.score <20:
            return "You Can See Into The Future. What Can You See?"
        elif self.score <30:
            return "A hero is born ? But that person is not you...obviously :p"
        elif self.score <40:
            return "Instoppable °0°...Just kidding"
        elif self.score <50:
            return "Are you even human at all ?"
        elif self.score <60:
            return "I must say, I am impressed by your non-existant skills"
        elif self.score <70:
            return "You may be quick but you can't beat us !"
        elif self.score <90:
            return "You almost made me sweat...I mean, AI don't sweat, do they ?"
        elif self.score <100:
            return "This is impossible ! Are you cheating ?"
        else:
            return "I surrender, your majesty. I am your loyal servent"


    def checkGameOver(self):
        """check and handle for a game over state"""
        if self.player.health <= 0 :
            if self.playSoundFx :
                self.soundDeath.volume = self.volumeSoundFX
                self.soundDeath.play()
            self.handleGameOver()


    def checkCollisions(self, dt):
        """ Move the player and handle any collision"""
        #Detect sphere/wall collisions
        collisions = self.player.hitWalls() + self.target.hitWalls()
        for i in range( len(self.rocks) ):
            for collision in self.rocks[i].hitWalls():
                collisions.append(collision)
        #Detect sphere/sphere collisions
        for collision in self.player.hitSphere(self.target):
            collisions.append(collision)

        for i in range( len(self.rocks) ):
            for collision in self.player.hitSphere(self.rocks[i]):
                collisions.append(collision)
        #Solve Wall collisions
        for collision in collisions :
            if collision["ID"]==1:
                self.handleTouchWalls(collision['particle'], collision["wall"])
            if collision["ID"]==2:
                if collision["other"].ID =="target":
                    self.handleTouchTarget()
                else:
                    self.handleTouchRock(collision["other"])
        # Complementary update
        for i in range(len(self.rocks)):
            self.rocks[i].updateState(dt)
        self.player.updateState(dt)
        self.target.updateState(dt)


    def handleTouchWalls(self, particle, wall):
        """  Handle a sucessful capture of the target """
        particle.solveWallsCollision(wall)


    def handleTouchTarget(self):
        """  Handle a sucessful capture of the target """
        self.updateScore(1)
        self.canvas.remove(self.target.instruction )
        self.target = Target(gravity=self.gravity, radius=0.05, canvas=self.canvas, level=self.level)
        if self.playSoundFx :
            self.soundTarget.volume = self.volumeSoundFX
            self.soundTarget.play()


    def handleTouchRock(self, other):
        """  Handle a sucessful capture of the target """
        self.player.solveSpheresCollision(other)


    def handleGameOver(self):
        """ handle a game over state """
        Clock.unschedule(self.update)
        self.soundBGM.volume = 0.1
        if not self.gameOverMenu:
            self.gameOverMenu = MDDialog(
                #title="Starship destoyed",
                text = "",
                size_hint=(0.8, 1),
                auto_dismiss=False,
                buttons = [
                    MDRoundFlatButton(text="Try Again", on_release=self.handleRetry),
                    MDRoundFlatButton(text="Stages", on_release=self.handleStages),
                    MDRoundFlatButton(text="Exit", on_release=self.handleExit),
                    ],
                )
        points = "{} point".format(self.score) if self.score<=1 else "{} points".format(self.score)
        performance = "Z4rka-sM3 : " + self.getPerformance()
        self.gameOverMenu.text  ="You earned {}\n\n{}".format(points, performance)
        self.gameOverMenu.open()


    def handlePause(self):
        """ handle pausing the game """
        Clock.unschedule(self.update)
        self.soundBGM.volume = 0.1
        if not self.pauseMenu:
            self.pauseMenu = MDDialog(
                #title="Game Paused",
                text= "Z4rka-sM3 : Pausing is for the skill-less",
                size_hint=(0.8, 1),
                auto_dismiss=False,
                buttons = [
                    MDRoundFlatButton(text="Resume", on_release=self.handleResume),
                    MDRoundFlatButton(text="Stages", on_release=self.handleStages),
                    MDRoundFlatButton(text="Exit", on_release=self.handleExit),
                    ]
                )
        self.pauseMenu.open()


    def handleResume(self, *args):
        """ handle resuming the game """
        if self.pauseMenu : self.pauseMenu.dismiss()
        if self.gameOverMenu : self.gameOverMenu.dismiss()
        self.soundBGM.volume = self.volumeSoundBGM
        Clock.schedule_interval(self.update, 1./self.FPS)


    def handleRetry(self, *args):
        """ handle resetting the game """
        if self.pauseMenu : self.pauseMenu.dismiss()
        if self.gameOverMenu : self.gameOverMenu.dismiss()
        self.reset(self.context)
        Clock.schedule_interval(self.update, 1./self.FPS)


    def handleStages(self, *args):
        """ handle resetting the game """
        self.soundBGM.stop()
        self.canvas.clear()
        if self.pauseMenu : self.pauseMenu.dismiss()
        if self.gameOverMenu : self.gameOverMenu.dismiss()
        self.stagesCallBack()


    def handleExit(self, *args):
        """ handle game exit """
        self.soundBGM.stop()
        self.canvas.clear()
        if self.pauseMenu : self.pauseMenu.dismiss()
        if self.gameOverMenu : self.gameOverMenu.dismiss()
        self.exitCallBack()
