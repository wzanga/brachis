from kivy.graphics import Ellipse
from kivy.core.window import Window
from math import pi, cos, sin, sqrt
from random import random
import os


class Particle(object):
    def __init__(self, x=None, v=None, gravity=(0,0), radius=None, canvas=None, Cr=1, health=None, level=None, ID=""):
        """ Generator for a particle
            x  : position
            v  : velocity
            gravity : gravity dynamical parameters
            radius  : circle radius
            canvas  : place where the everything is drawn
            level   : game level
            ID  : particle identification (player, target, rock,...)
        """
        self.Cr = Cr
        self.gravity = gravity
        self.action = [-gravity[0], -gravity[1]]
        self.canvas = canvas
        self.hasReceivedInput = False
        self.instruction = Ellipse(source='', pos=(0, 0), size=(0,0))
        self.healthSource = os.path.join('images', 'health.png')
        self.level = level
        self.setHealth(health)
        self.setRadius(radius)
        self.setState(x,v)
        self.ID = ID


    def setRadius(self, value, debug=False):
        """ Set the size of the particle """
        if not False : value *=2
        self.radius = value * Window.width
        self.instruction.size = (2*self.radius, 2*self.radius)


    def setSource(self, value):
        """ Set the file containing the image of the particle  """
        self.source = value
        self.instruction.source = self.source


    def draw(self):
        """ canvas : place where the everything is drawn """
        self.canvas.add(self.instruction)


    def setPosition(self, value):
        """ Set a new value for the xition and update canvas """
        self.x = value
        self.instruction.pos = (self.x[0]-self.radius, self.x[1]-self.radius)


    def setVelocity(self, value):
        """ Set a new value for the velocity """
        self.v = value


    def setHealth(self, health, width=None, side=None):
        """ intialize the health bars """
        self.health = health


    def updateHealth(self, value):
        """ modify the value of the particle health """
        if value : self.health += value


    def getPositionBounds(self):
        """ Get the bounds for the position """
        wmin, wmax = (0, Window.width)
        hmin, hmax = (0, Window.height)
        r = self.radius
        return (wmin+r, wmax-r), (hmin+r, hmax-r)


    def setState(self, x=None, v=None):
        """ Set the xition and the velocity of the particle """
        if x==None:
            x=[0,0]
            (xmin, xmax), (ymin, ymax) = self.getPositionBounds()
            x[0] = xmin + (xmax-xmin) * random()
            x[1] = 10*ymin + (ymax-ymin) * random()
        if v==None:
            v=[0,0]
            speed = 5 + self.level * 10 * random()
            angle = 2 * pi * random()
            v[0] = speed * cos(angle)
            v[1] = speed * sin(angle)
        self.setPosition(x)
        self.setVelocity(v)


    def predictNextState(self, dt):
        """ return a prediction for the next state x(t+dt), v(t+dt) """
        dt = 10 * dt
        dtau = 1 * dt
        x0 = self.x[0] + 0.5 * (self.action[0] + self.gravity[0]) * (dtau**2) + self.v[0] * dt
        x1 = self.x[1] + 0.5 * (self.action[1] + self.gravity[1]) * (dtau**2) + self.v[1] * dt
        v0 = self.v[0] + (self.action[0] + self.gravity[0]) * dtau
        v1 = self.v[1] + (self.action[1] + self.gravity[1]) * dtau
        return (x0,x1),(v0,v1)


    def updateState(self, dt):
        """ update the position and velocity with  """
        x, v = self.predictNextState(dt)
        self.setState(x,v)


    def hitWalls(self):
        """ Detect particle-box collisions for a given particle
            output : collision[i] = (ti, collisionID, particle, wallID)
        """
        (xmin, xmax), (ymin, ymax) = self.getPositionBounds()
        collisions = []
        if self.x[0] <= xmin :
            collisions.append({'ID':1, 'particle':self, 'wall':"L"})
        if self.x[0] >= xmax :
            collisions.append({'ID':1, 'particle':self, 'wall':"R"})
        if self.x[1] <= ymin :
            collisions.append({'ID':1, 'particle':self, 'wall':"D"})
        if self.x[1] >= ymax :
            collisions.append({'ID':1, 'particle':self, 'wall':"U"})
        return collisions


    def solveWallsCollision(self, wall):
        """ Solve a regular sphere/wall collision """
        (xmin, xmax), (ymin, ymax) = self.getPositionBounds()
        (x0, x1) = self.x
        (v0, v1) = self.v
        if wall == "L" : (x0, v0) = (xmin, -v0 * self.Cr)
        if wall == "R" : (x0, v0) = (xmax, -v0 * self.Cr)
        if wall == "D" : (x1, v1) = (ymin, -v1 * self.Cr)
        if wall == "U" : (x1, v1) = (ymax, -v1 * self.Cr)
        self.setState((x0,x1), (v0,v1))
        self.updateHealth(-1)


    def hitSphere(self, other):
        """ check whether two particle overlap """
        dx = self.x[0] - other.x[0]
        dy = self.x[1] - other.x[1]
        R = self.radius + other.radius
        if dx**2 + dy**2 < R**2 :
            return [{'ID':2, 'particle':self,  'other': other}]
        return []


    def solveSpheresCollision(self, other):
        """ Solve a regular sphere/sphere collision """
        #Handle velocities update
        m1, m2 = self.radius**3, other.radius**3
        M = m1 + m2
        dx0 = self.x[0] - other.x[0]
        dx1 = self.x[1] - other.x[1]
        dv0 = self.v[0] - other.v[0]
        dv1 = self.v[1] - other.v[1]
        d2 = dx0**2 + dx1**2
        xdotv = (dv0 * dx0 + dv1 * dx1)
        u10 = self.v[0] - (2*m2/M) * xdotv / d2 * dx0
        u11 = self.v[1] - (2*m2/M) * xdotv / d2 * dx1
        u20 = other.v[0] + (2*m1/M) * xdotv / d2 * dx0
        u21 = other.v[1] + (2*m1/M) * xdotv / d2 * dx1
        #Energy loss
        u20 += (1-self.Cr) * u10
        u21 += (1-self.Cr) * u11
        u10 *= self.Cr
        u11 *= self.Cr
        #Hande positions update
        distance = sqrt(d2)
        n0 = dx0 / distance
        n1 = dx1 / distance
        epsilon = ((self.radius + other.radius) - distance) / 2.
        x10 = self.x[0] + epsilon * n0
        x11 = self.x[1] + epsilon * n1
        x20 = other.x[0] - epsilon * n0
        x21 = other.x[1] - epsilon * n1
        #updateScore
        self.setState(x=(x10, x11), v=(u10, u11))
        other.setState(x=(x20, x21), v=(u20, u21))
        self.updateHealth(-1)
        other.updateHealth(-1)


class Target(Particle):
    def __init__(self, gravity, radius=None, canvas=None, level=None):
        """ Generator for a target particle
            gravity : gravity dynamical parameters
            radius  : circle radius
            canvas  : place where the everything is drawn
            level   : game level
        """
        super().__init__(gravity=gravity, radius=radius, canvas=canvas, health=1, level=level, ID="target")
        self.setSource(os.path.join('images', 'target{}.png'.format(level)))
        self.setRandomVelocity()
        self.setRandomPosition()
        self.draw()


    def setRandomVelocity(self):
        """ set an initial low random velocity """
        speed = 2 * random()
        angle = 2 * pi * random()
        v0 = speed * cos(angle)
        v1 = speed * sin(angle)
        self.setVelocity((v0,v1))


    def setRandomPosition(self):
        """ set an initial bette random potision """
        (xmin, xmax), (ymin, ymax) = self.getPositionBounds()
        x0 = xmin + (xmax-xmin) * random()
        x1 = ymin + (ymax-ymin) * random()
        self.setPosition((x0,x1))


class Rock(Particle):
    def __init__(self, gravity, radius=None, canvas=None, level=None):
        """ Generator for a rock particle
            gravity : gravity dynamical parameters
            radius  : circle radius
            canvas  : place where the everything is drawn
            level   : game level
        """
        super().__init__(gravity=gravity, radius=radius, canvas=canvas, health=float("inf"), level=level, ID="rock")
        self.setSource(os.path.join('images', 'rock{}.png'.format(level)))
        self.draw()


class Player(Particle):
    def __init__(self, gravity, radius=None, canvas=None, level=None, spacecraftIdx=1):
        """ Generator for a player particle
            gravity : gravity dynamical parameters
            radius  : circle radius
            canvas  : place where the everything is drawn
            level   : game level
            spacecraftIdx : spacecraft model index
        """
        super().__init__(x=[Window.width/2., Window.height/4.], v=[0, 0], Cr=0.8, gravity=gravity, radius=radius, canvas=canvas, health=5, ID="player")
        self.setSource(os.path.join('images','spacecraft{}.png'.format(spacecraftIdx)))
        self.draw()


    def setHealth(self, health, width=80, side=80):
        """ intialize the health bars """
        self.health = health
        self.healthBars = [None] * self.health
        for i in range(self.health):
            self.healthBars[i] = Ellipse(source=self.healthSource, pos=(i*width, Window.height - 100), size=(side,side))
            self.canvas.add(self.healthBars[i])


    def updateHealth(self, value, width=80, side=80):
        """ modify the value of the particle health """
        self.health += value
        for i in range(abs(value)):
            if value<0 and self.healthBars :
                self.canvas.remove(self.healthBars.pop())
            else:
                pos = ((self.health-1)*width, Window.height - 100)
                health = Ellipse(source=self.healthSource, pos=pos, size=(side,side))
                self.healthBars.append(health)
                self.canvas.add(self.healthBars[-1])
