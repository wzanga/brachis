# [Brachistonaut] Spaceflight game with obstacles

# YouTube demo video
https://www.youtube.com/watch?v=UjX9ZA5pCnM


# In-game Snapshots
<p align="center">
    <img src=publish/images/icon.png  width="200" height="400">
    <img src=publish/images/level1A.png  width="200" height="400">
    <img src=publish/images/level1B.png  width="200" height="400">
    <img src=publish/images/level2A.png  width="200" height="400">
    <img src=publish/images/level2B.png  width="200" height="400">
    <img src=publish/images/lose.png  width="200" height="400">
    <img src=publish/images/stages.png  width="200" height="400">
    <img src=publish/images/settings.png  width="200" height="400">
</p>


# Developpement mode
 1. Use the 'publish/compilation.ipynb' to compile the code on GoogleColab
 2. fill in the buildozer.spec file : sources, data, etc....
 3. set android.arch = armeabi-v7a or  arm64-v8a depending of dev phone’s cpu (ex: “v7a” for redmi6A)
 4. android target api >= 29
 

**Resources and tutorials**
- https://towardsdatascience.com/3-ways-to-convert-python-app-into-apk-77f4c9cd55af
- https://towardsdatascience.com/github-action-that-automates-portfolio-generation-bc15835862dc
- https://stackoverflow.com/questions/59384085/how-to-debug-a-kivy-kivymd-app-for-android

 # Debug App with (adb)
 1. Enable debug mode on your smartphone
 2. Install adb from here https://pypi.org/project/adb/
 1. Get all running python process : adb logcat -s "python"
 2. Get the phone’s cpu-info: adb shell cat /proc/cpuinfo


 # Release mode
 1. set android.arch = arm64-v8a
 2. Compile the apk with the instructions the the 'publish/compilation.ipynb' file with option (buildozer -v android release)
 3. sign the apk with 'publish/Makefile' target
 4. align the apk with the publish/Makefile' and the approrpriate SDK build manager 

**Resources and tutorials**
- https://stackoverflow.com/questions/26828372/how-to-sign-a-modded-apk-on-mac-with-apktool
- https://androidsdkmanager.azurewebsites.net/Buildtools




